
import DOMUtil from 'zunzun/flyutil/dom.mjs'

import HTMLSection from './section.mjs'

export default class HTMLSections {

  constructor(divs, spacer, cfg) {
    let cb_script = document.createElement('script');
    cb_script.type = 'text/javascript';
    cb_script.textContent = `
      let FLYHTML_SECTION_LOADED = undefined;
    `;
    document.head.appendChild(cb_script);

    this.queue = [];
    for (let div of divs) {
      this.queue.push(new HTMLSection(div));
    }
    this.spacer = spacer;
    this.cfg = cfg || {};
    if (typeof this.cfg.distance != "number") {
      this.cfg.distance = window.innerHeight || document.documentElement.clientHeight;
    }

    const _this = this;
    const ck_scroll = (evt) => {
      _this.update();
    }
    document.addEventListener('scroll', ck_scroll);
    this.update();
  }

  async update() {
    try {
      if (this.queue.length > 0) {
        const next_distance = DOMUtil.distance_to_viewport(this.queue[0].div);
        if (next_distance < this.cfg.distance && !this.queue[0].loading && !this.queue[0].loaded) {
          if (this.spacer) {
            this.queue[0].div.style.display = "none";
            if (this.queue[0].div.nextSibling) {
              this.queue[0].div.parentNode.insertBefore(this.spacer, this.queue[0].div.nextSibling);
            } else {
              this.queue[0].div.parentNode.appendChild(this.spacer);
            }
          }
          await this.queue[0].load();
          this.update();
        } else if (this.queue[0].loaded) {
          if (this.cfg.delay) {
            const _this = this;
            await new Promise((resolve) => {
              setTimeout(() => { resolve() }, _this.cfg.delay);
            });
          }
          if (this.spacer) {
            this.queue[0].div.style.display = "";
            this.queue[0].div.parentNode.removeChild(this.spacer);
          }
          if (this.queue.length > 0) {
            this.queue.shift();
            this.update();
          } else if (typeof this.cfg.on_done == "function") {
            this.cfg.on_done();
          }
        }
      }
    } catch (e) {
      console.error(e);
    }
  }

}
