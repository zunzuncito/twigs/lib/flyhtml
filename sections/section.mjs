

export default class HTMLSection {
  constructor(div) {
    this.div = div;
    this.loaded = false;
    this.loading = false;
  }

  async load() {
    try {
      console.debug("Load section:",  this.div.getAttribute("src"));
      this.loading = true;
      let await_call = this.div.getAttribute("await-call");
      if (typeof await_call == "string") await_call = true;

      const content_src = this.div.getAttribute("src");
      const section_html = await (await fetch(content_src)).text();
      this.div.innerHTML += section_html;


      const links = this.div.querySelectorAll("link");
      for (let link of links) {
        link.onload = (e) => {
          link.section_item_loaded = true;
        };
      }

      const imgs = this.div.querySelectorAll("img");
      for (let img of imgs) {
        img.onload = (e) => {
          img.section_item_loaded = true;
        };
      }

      let scripts = undefined;
      await new Promise(async (fulfil) => {
        if (await_call) {
          FLYHTML_SECTION_LOADED = () => {
            fulfil()
          }
        }

        scripts = this.div.querySelectorAll("script");
        for (let script of scripts) {
          let re_script = document.createElement('script');
          re_script.type = 'text/javascript';

          if (typeof script.src == "string" && script.src.length > 0) {
            re_script.src = script.src;
            re_script.onload = (e) => {
              script.section_item_loaded = true;
            };
          } else {
            re_script.textContent = script.textContent;
            script.section_item_loaded = true;
          }

          script.parentNode.replaceChild(re_script, script);
        }

        if (!await_call) {
          fulfil()
        }
      });

      const _this = this;

      const ck_loaded = () => {
        for (let script of scripts) {
          if (!script.section_item_loaded) return false;
        }
        for (let link of links) {
          if (!link.section_item_loaded) return false;
        }
        for (let img of imgs) {
          if (!img.section_item_loaded) return false;
        }

        return true;
      }
      await new Promise((resolve) => {
        const ck_interval = setInterval(() => {
          if (ck_loaded()) {
            clearInterval(ck_interval);
            _this.loading = false;
            _this.loaded = true;
            resolve();
          }
        }, 100);
      });
    } catch (e) {
      console.error(e);
    }
  }
}
