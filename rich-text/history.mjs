

function generate_query_selector(element, ancestor) {
  const selectors = [];
  let current = element;

  if (current.nodeType !== Node.ELEMENT_NODE) {
    current = current.parentNode;
  }
  
  while (current !== ancestor) {
    if (!current) {
      // If the current element becomes null, it means the element is not a descendant of the ancestor.
      return null;
    }
    
    const tag_name = current.tagName.toLowerCase();
    const index = Array.from(current.parentNode.children).indexOf(current) + 1;
    selectors.unshift(`${tag_name}:nth-child(${index})`);
    current = current.parentNode;
  }

  return selectors.join(" > ");
}



export default class EditorHistory {
  constructor(editor) {
    this.stack = [];
    this.cursor_stack = [];
    this.current_index = -1;
    this.editor = editor;
  }

  capture_state() {
    console.log("capture_state", this.current_index);
    const content = this.editor.input.innerHTML;
    if (content !== this.stack[this.current_index]) {
      if (this.current_index < this.stack.length - 1) {
        this.stack.splice(this.current_index + 1);
        this.cursor_stack.splice(this.current_index + 1);
      }
      this.stack.push(content);

      const selection = window.getSelection();
      if (selection.rangeCount > 0) {
        const range = selection.getRangeAt(0);
        this.cursor_stack.push({
          qstr: generate_query_selector(range.startContainer, this.editor.input),
          offset: range.startOffset
        });
      }

      this.current_index++;
    }
  }

  undo() {
    if (this.current_index > 0) {
      this.current_index--;
      this.editor.input.innerHTML = this.stack[this.current_index];
      this.restore_cursor();
    }
  }

  redo() {
    if (this.current_index < this.stack.length - 1) {
      this.current_index++;
      this.editor.input.innerHTML = this.stack[this.current_index];
      this.restore_cursor();
    }
  }

  restore_cursor() {
    const cursor_state = this.cursor_stack[this.current_index];
    const selection = window.getSelection();
    if (selection.rangeCount > 0) {
      const range = selection.getRangeAt(0);
      const container = this.editor.input.querySelector(cursor_state.qstr);
      console.log(container);
      const container_tn = container.firstChild;
      if (container_tn) {
        range.setStart(container_tn, cursor_state.offset);
        range.setEnd(container_tn,  cursor_state.offset);
      } else {
        range.setStart(container, cursor_state.offset);
        range.setEnd(container, cursor_state.offset);
      }
    }
  }
}
