
import EditorOption from './option.mjs';
import AlignOption from './options/align.mjs';
import LinkOption from './options/link.mjs';
import ListOption from './options/list.mjs';
import MediaOption from './options/media.mjs';
import TableOption from './options/table.mjs';

import EditorHistory from './history.mjs';

import options_html from './options.html';


export default class RichTextEditor {
  constructor(element) {
    const _this = this;

    this.element = document.createElement("div");
    this.element.classList.add("rich-text-wrap");

    const parent_node = element.parentNode;
    parent_node.replaceChild(this.element, element);

    this.input = element;

    this.element.innerHTML = options_html;

    this.history = new EditorHistory(this);

    element.addEventListener("focus", (e) => {
      _this.prepare();
      _this.element.querySelector("div.rec-media").removeAttribute("disabled");
    });

    element.addEventListener("keyup", (e) => {
      if (element.innerHTML.length == 0) element.innerHTML = "<div></div>";
      _this.element.querySelector("div.rec-media").setAttribute("disabled", true);
    });

    element.addEventListener("keydown", (e) => {
      if (e.key === "Z" && e.ctrlKey && e.shiftKey) {
        e.preventDefault();
        _this.history.redo();
        return;
      }

      if (e.key === "z" && e.ctrlKey) {
        e.preventDefault();
        _this.history.undo();
        return;
      }

      _this.history.capture_state();
    });

    element.addEventListener("input", (e) => {
      _this.prepare();
    });


    let rec_options = this.element.querySelectorAll("div.rec-option");
    for (let rec_option of rec_options) {
      const align = rec_option.getAttribute("align");
      if (align) {
        new AlignOption(rec_option, this);
        continue;
      }

      const state = rec_option.getAttribute("state");
      if (
        state == "wide" ||
        state == "full"
      ) {
        rec_option.addEventListener("click", (e) => {
          if (state == "wide") {
            if (_this.element.classList.contains("wide")) {
              _this.element.classList.remove("wide")
            } else {
              _this.element.classList.add("wide")
            }
          }

          if (state == "full") {
            if (_this.element.classList.contains("full")) {
              _this.element.classList.remove("full")
            } else {
              _this.element.classList.add("full")
            }
          }
        });
      } else if (
        state == "bullet_list" ||
        state == "ordered_list"
      ) {
        new ListOption(rec_option, this);
      } else if (
        state == "link"
      ) {
        new LinkOption(rec_option, this);
      } else if (
        state == "table"
      ) {
        new TableOption(rec_option, this);
      } else  if (
        state == "media"
      ) {
        if (state == "media") this.media_select = rec_option;
        new MediaOption(rec_option, this);
      } else {
        new EditorOption(rec_option, this);
      }
    }

    this.element.appendChild(element);
  }

  wrap_loose_brs() {
    for (let ecn of this.input.childNodes) {
      if (
        ecn.nodeType === Node.ELEMENT_NODE &&
        ecn.tagName.toLowerCase() == "br"
      ) {
        console.log("WRAP LOOSE BR");
        const wrap_div = document.createElement("div");
        ecn.parentNode.replaceChild(wrap_div, ecn);
        wrap_div.appendChild(ecn);
      }
    }
  }

  prepare() {
    if (
      this.input.innerHTML.length == 0 ||
      (
        this.input.childNodes.length == 1 &&
        this.input.firstChild.tagName.toLowerCase() == "br"
      )
    ) {
      this.input.innerHTML = "<div></div>";
      const selection = window.getSelection();
      if (selection.rangeCount > 0) {
        const range = selection.getRangeAt(0);
        range.setStart(this.input.firstChild, 0);
        range.setEnd(this.input.firstChild, 0);
      }
    }
  }
}
