
import EditorOption from '../option.mjs';

export default class MediaOption extends EditorOption {

  constructor(element, editor) {
    super(element, editor, async (range) => {
      const media_items = await editor.media_cb();
      for (let mitem of media_items) {
        console.log("handle", mitem);
        if (mitem.match(/mp4$/)) {
          const stateElement = document.createElement("div");
          stateElement.setAttribute("contenteditable", true);
          stateElement.classList.add("resize-media");
          stateElement.controls = "controls";
          const mp4_src = mitem;
          const ogv_src = mitem.replace(/mp4$/, "ogv");
          const webm_src = mitem.replace(/mp4$/, "webm");
          stateElement.innerHTML = `
<video controls>
<source type="video/mp4" src="${mp4_src}"></source>
<source type="video/ogg" src="${ogv_src}"></source>
<source type="video/webm" src="${webm_src}"></source>
</video>
          `;
          range.insertNode(stateElement);
        } else if (mitem.match(/mp3$/)) {
          const stateElement = document.createElement("audio");
          stateElement.controls = "controls";
          const mp3_src = mitem;
          const ogg_src = mitem.replace(/mp3$/, "ogg");
          const aac_src = mitem.replace(/mp3$/, "aac");
          stateElement.innerHTML = `
<source type="audio/mp3" src="${mp3_src}"></source>
<source type="audio/ogg" src="${ogg_src}"></source>
<source type="audio/aac" src="${aac_src}"></source>
          `;
          range.insertNode(stateElement);
        } else {
          const stateElement = document.createElement("div");
          stateElement.setAttribute("contenteditable", true);
          stateElement.classList.add("resize-media");
          stateElement.innerHTML = `
<img src="${mitem}" />
          `;
          range.insertNode(stateElement);
          console.log("image", stateElement);
        }
      }
    });
  }
}
