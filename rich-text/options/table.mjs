
import EditorOption from '../option.mjs';

export default class TableOption extends EditorOption {

  constructor(element, editor) {
    super(element, editor, (range) => {
      const new_table = document.createElement("div");
      new_table.classList.add("table");
      new_table.innerHTML = `
<div class="tr">
  <div class="td"><div>
  </div></div>
  <div class="td"><div>

  </div></div>
</div>
              `;
      range.insertNode(new_table);
    });
  }
}
