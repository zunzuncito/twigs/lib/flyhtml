
import EditorOption from '../option.mjs';

export default class LinkOption extends EditorOption {

  constructor(element, editor) {
    super(element, editor, (range) => {
      const stateElement = document.createElement("a");
      stateElement.href = prompt("Link URL:");
      stateElement.target = "_blank";

      if (range.startOffset === range.endOffset) {
        range.insertNode(stateElement);
      } else {
        range.surroundContents(stateElement);
      }
    });
  }
}
