
import EditorOption from '../option.mjs';

const state_elems = {
  bullet_list: "ul",
  ordered_list: "ol"
}

function fully_within_range(range, node) {
  const node_range = document.createRange();
  node_range.selectNode(node);

  return range.compareBoundaryPoints(Range.START_TO_START, node_range) <= 0 &&
         range.compareBoundaryPoints(Range.END_TO_END, node_range) >= 0;
}

export default class ListOption extends EditorOption {

  constructor(element, editor) {
    super(element, editor, (range, selection) => {
      const state = element.getAttribute("state");

      let ancestor = range.commonAncestorContainer.nodeType == 1 ?
        range.commonAncestorContainer :
        range.commonAncestorContainer.parentNode;

      console.log(ancestor);

      if (!editor.input.contains(ancestor)) return;

      let repl_ancestor = undefined;
      if (editor.input == ancestor) {
        repl_ancestor = document.createElement("div");

      }

      const list_divs = [];

      const rangeRects = range.getClientRects();
      const walker = document.createTreeWalker(ancestor, NodeFilter.SHOW_ELEMENT);
      while (walker.nextNode()) {
        const cnode = walker.currentNode;
        if (
          range.intersectsNode(cnode) &&
          cnode.tagName.toLowerCase() == "div"
        ) {
          list_divs.push(cnode);
        }
      }

      console.log("LDF", list_divs);

      if (repl_ancestor) {
        if (list_divs.length > 0) {
          ancestor.insertBefore(repl_ancestor, list_divs[0]);
          ancestor = repl_ancestor;
        } else {
          if (ancestor.firstChild) { 
            ancestor = ancestor.firstChild;
          } else {
            ancestor.appendChild(repl_ancestor);
            ancestor = repl_ancestor;
          }
        }
      }

      console.log(ancestor);

      if (
        list_divs.length == 0 &&
        ancestor.tagName.toLowerCase() == "div" &&
        ancestor !== element
      ) {
        list_divs.push(ancestor);
      }

      console.log("LDS", list_divs);

      const stateElement = document.createElement(state_elems[state]);
      list_divs[0].parentNode.insertBefore(stateElement, list_divs[0]);
      const list_items = [];
      for (let ldiv of list_divs) {
        const list_item = document.createElement("li");
        list_item.innerHTML = ldiv.innerHTML;
        stateElement.appendChild(list_item);
        list_items.push(list_item);
        ldiv.parentNode.removeChild(ldiv);
      }

      const cursor_li = list_items[list_items.length-1];
      const cursor_li_tn = cursor_li.firstChild;
      if (cursor_li_tn) {
        range.setStart(cursor_li_tn, cursor_li_tn.length);
        range.setEnd(cursor_li_tn,  cursor_li_tn.length);
      } else {
        range.setStart(cursor_li, 0);
        range.setEnd(cursor_li,  0);
      }
//      range.collapse(true);
//      selection.removeAllRanges();
//      selection.addRange(range);
//      editor.input.focus();
    });
  }
}
