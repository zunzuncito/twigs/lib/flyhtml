
import EditorOption from '../option.mjs';

const align_h = [
  "left", "hcenter", "right"
]

const align_v = [
  "top", "vcenter", "bottom"
]


function find_ancestor(element, class_name) {
  let current_element = element;

  while (current_element) {
    if (current_element.classList.contains(class_name)) {
      return current_element;
    }

    current_element = current_element.parentElement;
  }

}


export default class AlignOption extends EditorOption {

  constructor(element, editor) {
    super(element, editor, (range) => {
      const align = element.getAttribute("align");

      let ancestor = range.commonAncestorContainer.nodeType == 1 ?
        range.commonAncestorContainer :
        range.commonAncestorContainer.parentNode;

      if (!editor.input.contains(ancestor)) return;

      if (editor.input == ancestor) {
        if (ancestor.firstChild.classList.contains("align-div")) {
          console.log("ALREADY WRAPPED");
          ancestor = ancestor.firstChild;
        } else {
          const wrap_div = document.createElement("div");
          wrap_div.innerHTML = ancestor.innerHTML;
          ancestor.innerHTML = "";
          editor.input.appendChild(wrap_div);
          ancestor = wrap_div;
        }
      }

      const td_anc = find_ancestor(ancestor, "td")
      if (td_anc) {
        td_anc.classList.add("align-div");
        ancestor = td_anc;
      } else {
        let align_div = find_ancestor(ancestor, "align-div")
        if (!align_div) {
          align_div = document.createElement("div");
          align_div.classList.add("align-div");
          ancestor.parentNode.replaceChild(align_div, ancestor);
          align_div.appendChild(ancestor);
        }
        ancestor = align_div;
      }


      if (align_h.includes(align)) {
        for (let ah of align_h) {
          console.log(ah);
          if (align == ah) {
            console.log("add", align);
            ancestor.classList.add(align);
          } else if (ancestor.classList.contains(ah)) {
            ancestor.classList.remove(ah);
          }
        }
      } else {

        for (let av of align_v) {
          if (align == av) {
            ancestor.classList.add(align);
          } else if (ancestor.classList.contains(av)) {
            ancestor.classList.remove(av);
          }
        }
      }

    });
  }
}
