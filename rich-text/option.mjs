
function intersects_range(element, range_rects) {
  const element_rect = element.getBoundingClientRect();
  for (const range_rect of range_rects) {
    if (
      range_rect.left <= element_rect.right &&
      range_rect.right >= element_rect.left &&
      range_rect.top <= element_rect.bottom &&
      range_rect.bottom >= element_rect.top
    ) {
      return true;
    }
  }
  return false;
}

function fully_within_range(range, node) {
  const node_range = document.createRange();
  node_range.selectNode(node);

  return range.compareBoundaryPoints(Range.START_TO_START, node_range) <= 0 &&
         range.compareBoundaryPoints(Range.END_TO_END, node_range) >= 0;
}

function html_removal(fragment) {
  for (var i = 0; i < fragment.childNodes.length; i++) {
    var node = fragment.childNodes[i];
    if (node.nodeType === Node.ELEMENT_NODE) {
      if (node.tagName.toLowerCase() === "div") {
        html_removal(node);
      } else {
        html_removal(node);
        const nfrag = document.createDocumentFragment();
        nfrag.append(...node.childNodes);
        fragment.replaceChild(nfrag, node);
      }
    }
  }
}

function tag_removal(fragment, tag) {
  console.log(fragment, "remove", tag);
  for (var i = 0; i < fragment.childNodes.length; i++) {
    var node = fragment.childNodes[i];
    if (node.nodeType === Node.ELEMENT_NODE) {
      if (node.tagName.toLowerCase() === tag) {
        console.log(node, "is", tag);
        const nfrag = document.createDocumentFragment();
        nfrag.append(...node.childNodes);
        fragment.replaceChild(nfrag, node);
        tag_removal(nfrag, tag);
      } else {
        console.log(node, "ckchildren");
        tag_removal(node, tag);
      }
    }
  }
}

const state_elems = {
  bold: "b",
  italic: "i",
  code: "code",
  quote: "blockquote",
  hrule: "hr"
}

export default class EditorOption {

  constructor(element, editor, extension_cb) {
    this.element = element;

    const state = element.getAttribute("state");

    element.addEventListener("mousedown", async (e) => {
      e.preventDefault();

      const selection = window.getSelection();
      if (
        editor.input.contains(selection.anchorNode) &&
        editor.input.contains(selection.focusNode)
      ) {
        const range = selection.getRangeAt(0);

        let intersecting_elems = [];

        const ancestor = range.commonAncestorContainer.nodeType == 1 ?
          range.commonAncestorContainer :
          range.commonAncestorContainer.parentNode;

        const rang_rects = range.getClientRects();


        if (state === "clear") {
          const selected_nodes = range.extractContents();
          html_removal(selected_nodes);
          range.insertNode(selected_nodes);
          return;
        }


        if (extension_cb) {
          extension_cb(range, selection);
        } else {
          const stateElement = document.createElement(state_elems[state]);
          if (state == "hrule") {
            range.insertNode(stateElement);
          } else if (range.startOffset !== range.endOffset) {
            const selected_nodes = range.extractContents();
            tag_removal(selected_nodes, state_elems[state]);
            stateElement.appendChild(selected_nodes);
            range.insertNode(stateElement);
          }
        }

        editor.history.capture_state();
      }
    });

  }

}
