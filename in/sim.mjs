
export default class InputSimulator {
  static click(element) {
    const event = new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: true
    });
    element.dispatchEvent(event);
  }

  static type(element, text) {
    element.focus();
    text.split('').forEach(char => {
      const event = new KeyboardEvent('keydown', { key: char, bubbles: true });
      element.dispatchEvent(event);
      element.value += char;
      const inputEvent = new Event('input', { bubbles: true });
      element.dispatchEvent(inputEvent);
    });
  }

  static press_key(element, key) {
    let keyCode = key.length === 1 ? key.charCodeAt(0) : {
      'Enter': 13,
      'Tab': 9,
      'Backspace': 8,
      'Escape': 27,
      'ArrowUp': 38,
      'ArrowDown': 40,
      'ArrowLeft': 37,
      'ArrowRight': 39,
      // Add more key codes if needed
    }[key];

    if (keyCode === undefined) {
      console.error(`Unsupported key: ${key}`);
      return;
    }

    // Simulate keydown
    let keydownEvent = new KeyboardEvent('keydown', {
      key: key,
      code: `Key${key.toUpperCase()}`,
      keyCode: keyCode,
      which: keyCode,
      bubbles: true,
      cancelable: true
    });
    element.dispatchEvent(keydownEvent);

    // Simulate keypress (only for printable keys)
    if (key.length === 1) {
      let keypressEvent = new KeyboardEvent('keypress', {
        key: key,
        code: `Key${key.toUpperCase()}`,
        keyCode: keyCode,
        which: keyCode,
        bubbles: true,
        cancelable: true
      });
      element.dispatchEvent(keypressEvent);
    }

    // Simulate keyup
    let keyupEvent = new KeyboardEvent('keyup', {
      key: key,
      code: `Key${key.toUpperCase()}`,
      keyCode: keyCode,
      which: keyCode,
      bubbles: true,
      cancelable: true
    });
    element.dispatchEvent(keyupEvent);
  }
}
