
import body_html from './body.html';

export default class {
  constructor(cfg, steps, donecb) {
    this.element = document.createElement("div");
    this.element.innerHTML = body_html;

    if (!cfg) cfg = {};

    this.step_loading = this.element.querySelector("div.step_loading");
    this.step_form = this.element.querySelector("div.step_form");

    this.instruction_block = this.step_form.querySelector("h3");
    if (cfg.instructions) {
      this.instruction_block.innerText = cfg.instructions;
    } else {
      this.element.removeChild(this.instruction_block);
    }

    this.error_block = this.step_form.querySelector(".err_msg");

    this.button_back = this.step_form.querySelector("button");

    this.step_input = this.step_form.querySelector("input");

    this.button_forward = this.step_form.querySelector(".step_form_inputs button:nth-child(3)");

    let _this = this;
    this.step_input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        _this.button_forward.click();
      }
    });

    this.display_loading = function() {
      _this.step_form.style.display = "none";
      _this.step_loading.style.display = "";
    }

    this.hide_loading = function() {
      _this.step_form.style.display = "";
      _this.step_loading.style.display = "none";
    }

    this.steps = steps;

    let form_values = {};
    let tmp_values = {};

    let nonamei = 0;
    for (let s = 0; s < steps.length; s++) {
      let step = steps[s];
      if (!step.name) {
        step.iname = "noname"+nonamei;
        nonamei++;
      }
    }

    function error_callback(msg) {
      _this.error_block.innerText = msg;
    }
    this.err_cb = error_callback;

    let csi = 0;
    let prepare_step = this.prepare_step = function() {
      if (csi > 0) {
        _this.step_input.style.marginLeft = "";
        _this.button_back.removeAttribute("disabled");
      } else {
//        _this.step_input.style.marginLeft = _this.button_back.offsetWidth+"px";
        _this.button_back.setAttribute("disabled", true);
      }

      let step = steps[csi];
      _this.step_input.placeholder = step.placeholder;
      _this.step_input.type = step.type;
      if (step.name && form_values[step.name]) {
        _this.step_input.value = form_values[step.name];
      } else if (tmp_values[step.iname]) {
        _this.step_input.value = tmp_values[step.iname];
      }

      if (typeof step.dom === 'function') step.dom(_this.element, (skip_val) => {
        csi += skip_val;
        prepare_step();
      })

      _this.hide_loading();
      _this.step_input.focus();
    }

    this.button_back.addEventListener("click", async function(e) {
      try {
        let step_value = _this.step_input.value;
        form_values[steps[csi].name] = step_value;
        console.log("form_values", form_values);
        csi--;
        if (typeof steps[csi].do_skip == "function" && await steps[csi].do_skip()) csi--;
        prepare_step();
      } catch (e) {
        console.error(e.stack);
      }
    });

    this.button_forward.addEventListener("click", async function(e) {
      try {
        let step_value = _this.step_input.value;
        if (await steps[csi].verify(step_value, form_values, error_callback, _this.element)) {

          if (steps[csi].name) {
            form_values[steps[csi].name] = step_value;
          } else {
            tmp_values[steps[csi].iname] = step_value;
          }
          csi++;


          if (csi < steps.length && typeof steps[csi].do_skip == "function" && await steps[csi].do_skip()) {
            csi++;
          }

          if (csi >= steps.length) {
            if ( !(await donecb(form_values)) ) {
              csi--;
              if (typeof steps[csi].do_skip == "function" && await steps[csi].do_skip()) {
                csi--;
              } else {
                _this.step_input.value = "";
              }
              prepare_step();
            }
          } else {
            _this.step_input.value = "";
            prepare_step();
          }
        }
      } catch (e) {
        console.error(e);
      }
    });
    prepare_step();
  }
}
