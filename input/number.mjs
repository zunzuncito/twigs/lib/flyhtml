
export default class NumInput {
  constructor(element, fill_zeros) { 
    const _this = this;

    this.element = document.createElement("div");
    this.element.classList.add("num-input");

    element.label = element.getAttribute("label")
    if (element.label) {
      this.label = document.createElement("div")
      this.label.innerText = element.label;
      this.label.classList.add("label");
      this.element.appendChild(this.label);
    }

    this.do_fill_zeros = fill_zeros;

    element.parentNode.replaceChild(this.element, element);

    element.onkeypress = (e) => {
      const charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      } else {
        return true;
      }
    };

    if (!element.max) element.max = 9007199254740991;

    element.addEventListener("change", (e) => {
      const evalue = parseInt(element.value);
      if (evalue > parseInt(element.max)) {
        element.value = element.min;
      } else if (evalue < parseInt(element.min)) {
        element.value = element.max;
      }

      if (element.value.toString().length > element.max.length) {
        element.value = element.value.slice(element.value.length-element.max.length, element.value.length);
      }

      _this.fill_zeros(e, element);
    });

    this.input = element;
    this.element.appendChild(this.input);

    this.controls = document.createElement("div");
    this.controls.classList.add("num-input-ctl");

    this.ctl_plus = document.createElement("div");
    this.ctl_plus.addEventListener("click", (e) => {
      const step = parseInt(element.step) || 1;
      if (parseInt(element.value) + step <=  parseInt(element.max)) {
        element.value = parseInt(element.value) + step;
      } else {
        element.value = element.min;
      }
      const event = new Event('change');
      element.dispatchEvent(event);
      _this.fill_zeros({ target: element }, element);
    });
    this.ctl_plus.classList.add("num-input-ctl-plus");
    this.ctl_plus.innerText = "+";
    this.controls.appendChild(this.ctl_plus);

    this.ctl_minus = document.createElement("div");
    this.ctl_minus.addEventListener("click", (e) => {
      const step = element.step || 1;
      if (parseInt(element.value) - step >=  parseInt(element.min)) {
        element.value = parseInt(element.value) - step;
      } else {
        element.value = Math.floor(element.max/step)*step;
      }
      const event = new Event('change');
      element.dispatchEvent(event);
      _this.fill_zeros({ target: element }, element);
    });
    this.ctl_minus.classList.add("num-input-ctl-minus");
    this.ctl_minus.innerText = "-";
    this.controls.appendChild(this.ctl_minus);

    this.element.appendChild(this.controls);

  }

  fill_zeros(e, element) {
    if (this.do_fill_zeros) {
      for (let i = 0; i < element.max.length-e.target.value.length; i++) {
        e.target.value = "0"+e.target.value
      }
    }
  }
}
