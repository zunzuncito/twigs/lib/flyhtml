


export default class ConfirmInput {
  constructor(inputs, change_cb, valid_cb) {
    const _this = this;
    this.values = {};
    this.real_values = {};
    this.valid = {};
    for (let key in inputs) {
      const input = inputs[key];
      const check_valid = async (e) => {
        try {
          const value_key = e.target.name || e.target.getAttribute("name");
          _this.real_values[value_key] = e.target.real_value ||  e.target.value || e.target.getAttribute("value")
          _this.values[value_key] = e.target.value || e.target.getAttribute("value")
          _this.valid[value_key] = input.validate(_this.real_values[value_key]);

          if (_this.valid[value_key]) {
            if (e.target.classList.contains("invalid")) {
              e.target.classList.remove("invalid");
            }
          } else if (!e.target.classList.contains("invalid")) {
            e.target.classList.add("invalid");
          }

          let all_valid = true;
          for (let ikey in inputs) {
            if (!_this.valid[ikey]) all_valid = false;
          }


          await change_cb(e, _this);


          if (all_valid) {
            await valid_cb(_this);
          }
        } catch (err) {
          console.error(err);
        }
      }
      input.element.addEventListener("change", check_valid);

    }
  }

  static async construct(inputs, change_cb, valid_cb) {
    try {
      const _this = new ConfirmInput(inputs, change_cb, valid_cb);
      for (let key in inputs) {
        _this.real_values[key] = inputs[key].element.real_value ||  inputs[key].element.value || inputs[key].element.getAttribute("value")
        _this.values[key] = inputs[key].element.value || inputs[key].element.getAttribute("value");
        _this.valid[key] = inputs[key].validate(_this.real_values[key]);
        await change_cb({
          target: inputs[key].element
        }, _this);
      }

      return _this;
    } catch (e) {
      console.error(e);
    }
  }
}
