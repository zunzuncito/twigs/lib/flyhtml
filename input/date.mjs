
const WEEKDAYS = [
  "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"
]

export default class DateInput {
  constructor(element, step, duration) {
    const _this = this;

    const min = this.min_date = new Date(this.parse_date(element.getAttribute("min"))+"T00:00:00");
    const min_year = min.getFullYear();
    const min_month = min.getMonth();

    const max = this.max_date = new Date(this.parse_date(element.getAttribute("max"))+"T00:00:00");
    const max_year = max.getFullYear();
    const max_month = max.getMonth();

    const no_months = (max.getFullYear() - min.getFullYear()) * 12 - min.getMonth() + max.getMonth();

    this.selected_year = min.getFullYear();
    this.selected_month = min.getMonth();


    this.element = document.createElement("div");
    this.element.classList.add("date-input");


    this.input = document.createElement("input");
    this.input.type = "text";
    this.input.value = element.getAttribute("value");
    this.input.setAttribute("name", element.getAttribute("name"));
    if (typeof this.input.value == "string") this.input.value = this.input.value.replace(/\-/g, "/");
    this.input.placeholder = element.placeholder || element.getAttribute("placeholder");
    this.input.setAttribute("readonly", true);
    this.input.addEventListener("focus", (e) => {
      _this.element.appendChild(this.popup);
    });

    this.input.addEventListener("blur", (e) => {
      _this.element.removeChild(this.popup);
    });
    this.element.appendChild(this.input);


    this.popup = document.createElement("div")
    this.popup.classList.add("selection-popup");


    const year_input = document.createElement("div");
    year_input.classList.add("year-input");

    const prev_year = document.createElement("div");
    prev_year.classList.add("prev");
    prev_year.innerHTML = "<";
    prev_year.addEventListener("mousedown", (e) => {
      e.preventDefault();
      if (min_year <= _this.selected_year - 1) {
        _this.selected_year -= 1;
        cur_year.innerHTML = _this.selected_year;
        _this.selected_month = 11;
        cur_month.innerHTML = new Date(0, _this.selected_month+1, 0).toLocaleString('default', { month: 'long' });
        _this.update_days();
      }
    });
    year_input.appendChild(prev_year);

    const cur_year = document.createElement("div");
    cur_year.classList.add("current");
    cur_year.innerHTML = _this.selected_year;
    cur_year.addEventListener("mousedown", (e) => {
      e.preventDefault();
    });
    year_input.appendChild(cur_year);

    const next_year = document.createElement("div");
    next_year.classList.add("next");
    next_year.innerHTML = ">";
    next_year.addEventListener("mousedown", (e) => {
      e.preventDefault();
      if (_this.selected_year + 1 <= max_year) {
        _this.selected_year += 1;
        cur_year.innerHTML = _this.selected_year;
        _this.selected_month = 0;
        cur_month.innerHTML = new Date(0, _this.selected_month+1, 0).toLocaleString('default', { month: 'long' });
        _this.update_days();
      }
    });
    year_input.appendChild(next_year);

    this.popup.appendChild(year_input);

    
    const month_input = document.createElement("div");
    month_input.classList.add("month-input");

    const prev_month = document.createElement("div");
    prev_month.classList.add("prev");
    prev_month.innerHTML = "<";
    prev_month.addEventListener("mousedown", (e) => {
      e.preventDefault();
      const prev_year_available = min_year < _this.selected_year;
      let prev_month = _this.selected_month - 1;
      if (prev_month == -1) {
        if (!prev_year_available) return;
        _this.selected_year--;
        cur_year.innerHTML = _this.selected_year;
        prev_month = 11
      }

      if (min_month <= prev_month || prev_year_available) {
        _this.selected_month = prev_month;
        cur_month.innerHTML = new Date(0, _this.selected_month+1, 0).toLocaleString('default', { month: 'long' });
        _this.update_days();
      }
    });
    month_input.appendChild(prev_month);

    const cur_month = document.createElement("div");
    cur_month.classList.add("current");
    cur_month.innerHTML = new Date(0, this.selected_month+1, 0).toLocaleString('default', { month: 'long' });
    cur_month.addEventListener("mousedown", (e) => {
      e.preventDefault();
    });
    month_input.appendChild(cur_month);

    const next_month = document.createElement("div");
    next_month.classList.add("next");
    next_month.innerHTML = ">";
    next_month.addEventListener("mousedown", (e) => {
      e.preventDefault();
      const next_year_available = _this.selected_year < max_year;
      let next_month = _this.selected_month + 1;
      if (next_month == 12) {
        if (!next_year_available) return;
        _this.selected_year++;
        cur_year.innerHTML = _this.selected_year;
        next_month = 0
      }

      if (_this.selected_month <= max_month || next_year_available) {
        _this.selected_month = next_month;
        cur_month.innerHTML = new Date(0, _this.selected_month+1, 0).toLocaleString('default', { month: 'long' });
        _this.update_days();
      }
    });
    month_input.appendChild(next_month);

    this.popup.appendChild(month_input);

    const month_block = this.month_block = document.createElement("div");
    month_block.classList.add("date-input-month-block");
    this.popup.appendChild(month_block);

    this.update_days();

    

    element.parentNode.replaceChild(this.element, element);

  }

  
  update_days() {
    const _this = this;

    this.month_block.innerHTML = ""
    const selected_date = new Date(this.selected_year, this.selected_month, 0);




    for (let day = 0; day < 7; day++) {
      const day_block = document.createElement("div");
      day_block.classList.add("date-input-day-block", "weekday");
      day_block.innerHTML = WEEKDAYS[day];
      day_block.addEventListener("mousedown", (e) => {
        e.preventDefault();
      });
      this.month_block.appendChild(day_block);
    }


    let days_without_next = 0;

    const first_weekday = selected_date.getDay();

    const prev_month_date = new Date(this.selected_year, this.selected_month-1, 0);
    const start_date = new Date(this.selected_year, this.selected_month, -first_weekday);
    for (let day = first_weekday; day > 0; day--) {
      const day_block = document.createElement("div");
      day_block.classList.add("date-input-day-block");
      const day_num = prev_month_date.getDate()-day;
      day_block.innerHTML = day_num;
      day_block.addEventListener("mousedown", (e) => {
        _this.input.value = `${prev_month_date.getFullYear()}/${prev_month_date.getMonth()+2}/${day_num}`;
        const event = new Event("change");
        _this.input.dispatchEvent(event);
      });
      this.month_block.appendChild(day_block);
      days_without_next++;
    }

    const no_days = new Date(this.selected_year, this.selected_month + 1, 0).getDate();

    for (let day = 1; day <= no_days; day++) {
      const day_block = document.createElement("div");
      day_block.classList.add("date-input-day-block");
      day_block.innerHTML = day;
      day_block.addEventListener("mousedown", (e) => {
        _this.input.value = `${_this.selected_year}/${_this.selected_month+1}/${day}`;
        const event = new Event("change");
        _this.input.dispatchEvent(event);
      });
      this.month_block.appendChild(day_block);
      days_without_next++;
    }

    const next_month_date = new Date(this.selected_year, this.selected_month + 2, 0);

    let add_days = 42 - (first_weekday + no_days);
    const end_date = new Date(this.selected_year, this.selected_month + 1, add_days);

    for (let day = 1; day <= add_days; day++) {
      const day_block = document.createElement("div");
      day_block.classList.add("date-input-day-block");
      day_block.innerHTML = day;
      this.month_block.appendChild(day_block);
      day_block.addEventListener("mousedown", (e) => {
        _this.input.value = `${next_month_date.getFullYear()}/${next_month_date.getMonth()+1}/${day}`;
        const event = new Event("change");
        _this.input.dispatchEvent(event);
      });
    }
  }

  parse_date(dstr) {
    if (!dstr) return;
    let result = [];
    for(let num of dstr.split("-")) {
      if (num.length < 2) {
        result.push(`0${num}`);
      } else {
        result.push(num);
      }
    }
    return result.join("-");
  }

  validate(value) {
    const val_date_int = new Date(this.parse_date(value)).getTime();
    return this.min_date.getTime() <= val_date_int && val_date_int <= this.max_date.getTime();
  }
}
