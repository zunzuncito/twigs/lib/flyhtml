
const state_elems = {
  bold: "b",
  italic: "i",
  code: "code",
  link: "a",
  bullet_list: "ul",
  ordered_list: "ol",
  quote: "blockquote",
  hrule: "hr",
  table: "table"
}

const tf_elems = [
  "b", "i"
]

const align_h = [
  "left", "hcenter", "right"
]

const align_v = [
  "top", "vcenter", "bottom"
]

function findAncestor(element, class_name) {
  let currentElement = element.parentElement;

  while (currentElement) {
    if (currentElement.classList.contains(class_name)) {
      return currentElement;
    }

    currentElement = currentElement.parentElement;
  }

}

function intersectsRange(element, rangeRects) {
    const elementRect = element.getBoundingClientRect();

    // Check if any of the range's client rectangles intersect with the element's client rectangle
    for (const rangeRect of rangeRects) {
        if (
            rangeRect.left <= elementRect.right &&
            rangeRect.right >= elementRect.left &&
            rangeRect.top <= elementRect.bottom &&
            rangeRect.bottom >= elementRect.top
        ) {
            return true; // Intersection detected
        }
    }

    return false; // No intersection
}
        

export default class RichTextEditor {
  constructor(element) {
    const _this = this;

    this.element = document.createElement("div");
    this.element.classList.add("rich-text-wrap");

    const parent_node = element.parentNode;
    parent_node.replaceChild(this.element, element);

    element.addEventListener("focus", (e) => {
      if (element.innerHTML.length == 0) element.innerHTML = "<div></div>";
    });

    element.addEventListener("keyup", (e) => {
      if (element.innerHTML.length == 0) element.innerHTML = "<div></div>";
    });

    this.element.innerHTML = `
<div class="rich-editor-controls">
  <div class="rec-section">
    <div class="rec-option rec-bold" state="bold">&#xF101;</div>
    <div class="rec-option rec-italic" state="italic">&#xF102;</div>
    <div class="rec-option rec-code" state="code">&#xF103;</div>
    <div class="rec-option rec-link" state="link">&#xF104;</div>
  </div>
  <div class="rec-section">
    <div class="rec-option rec-ul" state="bullet_list">&#xF105;</div>
    <div class="rec-option rec-ol" state="ordered_list">&#xF106;</div>
    <div class="rec-option rec-quote" state="quote">&#xF107;</div>
  </div>
  <div class="rec-section">
    <div class="rec-option rec-hr" state="hrule">&#xF108;</div>
    <div class="rec-option rec-talbe" state="table">&#xF109;</div>
  </div>
  <div class="rec-section">
    <div class="rec-option rec-media" state="media" disabled=true>&#xF10A;</div>
  </div>
  <div class="rec-section">
    <div class="rec-option rec-wide" state="wide">&#xF10B;</div>
    <div class="rec-option rec-full" state="full">&#xF10C;</div>
  </div>
  <div class="rec-section">
    <div class="rec-option rec-align-left" align="left">&#xF10D;</div>
    <div class="rec-option rec-align-hcenter" align="hcenter">&#xF10E;</div>
    <div class="rec-option rec-align-right" align="right">&#xF10F;</div>
    <div class="rec-option rec-align-top" align="top">&#xF110;</div>
    <div class="rec-option rec-align-vcenter" align="vcenter">&#xF111;</div>
    <div class="rec-option rec-align-bottom" align="bottom">&#xF112;</div>
  </div>
</div>
    `
    element.addEventListener("focus", (e) => {
      _this.element.querySelector("div.rec-media").removeAttribute("disabled");
    });

    element.addEventListener("blur", (e) => {
      _this.element.querySelector("div.rec-media").setAttribute("disabled", true);
    });

    let rec_options = this.element.querySelectorAll("div.rec-option");
    for (let rec_option of rec_options) {

      const align = rec_option.getAttribute("align");
      const state = rec_option.getAttribute("state");

      if (state == "media") this.media_select = rec_option;
      rec_option.addEventListener("mousedown", async (e) => {
        e.preventDefault();



        if (state == "wide") {
          if (this.element.classList.contains("wide")) {

            this.element.classList.remove("wide")
          } else {
            this.element.classList.add("wide")
          }
          return;
        }

        if (state == "full") {
          if (this.element.classList.contains("full")) {
            this.element.classList.remove("full")
          } else {
            this.element.classList.add("full")
          }
          return;
        }

        const selection = window.getSelection();
        if (
          element.contains(selection.anchorNode) &&
          element.contains(selection.focusNode)
        ) {
          const range = selection.getRangeAt(0);
          console.log("range", range);


          console.log("align", align)
          if (align) {
            let ancestor = range.commonAncestorContainer.nodeType == 1 ?
              range.commonAncestorContainer :
              range.commonAncestorContainer.parentNode;

            if (element == ancestor || !element.contains(ancestor)) return;

            const td_anc = findAncestor(ancestor, "td")
            if (td_anc) {
              td_anc.classList.add("align-div");
              ancestor = td_anc;
            } else {
              let align_div = findAncestor(ancestor, "align-div")
              if (!align_div) {
                align_div = document.createElement("div");
                align_div.classList.add("align-div");
                ancestor.parentNode.replaceChild(align_div, ancestor);
                align_div.appendChild(ancestor);
              }
              ancestor = align_div;
            }

            console.log(align, ancestor);


            if (align_h.includes(align)) {
              for (let ah of align_h) {
                console.log(ah);
                if (align == ah) {
                  console.log("add", align);
                  ancestor.classList.add(align);
                } else if (ancestor.classList.contains(ah)) {
                  ancestor.classList.remove(ah);
                }
              }
            } else {

              for (let av of align_v) {
                if (align == av) {
                  ancestor.classList.add(align);
                } else if (ancestor.classList.contains(av)) {
                  ancestor.classList.remove(av);
                }
              }
            }

            return;
          }

          let intersecting_elems = [];
          const is_state = tf_elems.includes(state) ?
            document.queryCommandState(state) :
            (() => {
              const ancestor = range.commonAncestorContainer.nodeType == 1 ?
                range.commonAncestorContainer :
                range.commonAncestorContainer.parentNode;

              const rangeRects = range.getClientRects();

              const walker = document.createTreeWalker(ancestor, NodeFilter.SHOW_ELEMENT);
              while (walker.nextNode()) {
                const cnode = walker.currentNode;
                if (intersectsRange(cnode, rangeRects)) {
                  if (cnode.tagName.toLowerCase() == state_elems[state]) {
                    intersecting_elems.push(cnode);
                  } else if (
                    (state == "bullet_list" || state == "ordered_list") &&
                    cnode.tagName.toLowerCase() == "li"
                  ) {
                    intersecting_elems.push(cnode);
                  }
                }
              }

              return intersecting_elems.length > 0;
            })();

          console.log(state, is_state);

          if (is_state) {
            if (intersecting_elems.length > 0) {
              console.log(intersecting_elems);
              for (let i = intersecting_elems.length-1; i >= 0; i--) {
                const ielem = intersecting_elems[i];

                if (ielem.tagName.toLowerCase() == "li") {
                  const line_div = document.createElement('div');
                  line_div.innerHTML = ielem.innerHTML;
                  ielem.parentNode.replaceChild(line_div, ielem);
                } else {
                  const fragment = document.createDocumentFragment();
                  const tmp_div = document.createElement('div');
                  tmp_div.innerHTML = ielem.innerHTML;
                  while (tmp_div.firstChild) {
                    fragment.appendChild(tmp_div.firstChild);
                  }
                  ielem.parentNode.replaceChild(fragment, ielem);
                }
                selection.removeAllRanges();
            //    selection.addRange(range);
              }
            } else {
              document.execCommand(state, false, null);
            //  selection.removeAllRanges();
            }
          } else {
            if (state == "table") {
              const stateElement = document.createElement("div");
              stateElement.classList.add("table");
              stateElement.innerHTML = `
<div class="tr">
  <div class="td"><div>
  </div></div>
  <div class="td"><div>

  </div></div>
</div>
              `;
              range.insertNode(stateElement);
            } else if (state == "bullet_list" || state == "ordered_list") {
              const ancestor = range.commonAncestorContainer.nodeType == 1 ?
                range.commonAncestorContainer :
                range.commonAncestorContainer.parentNode;

              const list_divs = [];

              const rangeRects = range.getClientRects();
              const walker = document.createTreeWalker(ancestor, NodeFilter.SHOW_ELEMENT);
              while (walker.nextNode()) {
                const cnode = walker.currentNode;
                if (
                  intersectsRange(cnode, rangeRects) &&
                  cnode.tagName.toLowerCase() == "div"
                ) {
                  list_divs.push(cnode);
                }
              }

              if (
                list_divs.length == 0 &&
                ancestor.tagName.toLowerCase() == "div" &&
                ancestor !== element
              ) {
                list_divs.push(ancestor);
              }

              const stateElement = document.createElement(state_elems[state]);
              list_divs[0].parentNode.insertBefore(stateElement, list_divs[0]);
              for (let ldiv of list_divs) {
                const list_item = document.createElement("li");
                list_item.innerHTML = ldiv.innerHTML;
                stateElement.appendChild(list_item);
                ldiv.parentNode.removeChild(ldiv);
              }
            } else {
              if (
                state == "media"
              ) {
                const media_items = await _this.media_cb();
                for (let mitem of media_items) {
                  console.log("handle", mitem);
                  if (mitem.match(/mp4$/)) {
                    const stateElement = document.createElement("div");
                    stateElement.setAttribute("contenteditable", true);
                    stateElement.classList.add("resize-media");
                    stateElement.controls = "controls";
                    const mp4_src = mitem;
                    const ogv_src = mitem.replace(/mp4$/, "ogv");
                    const webm_src = mitem.replace(/mp4$/, "webm");
                    stateElement.innerHTML = `
<video controls>
  <source type="video/mp4" src="${mp4_src}"></source>
  <source type="video/ogg" src="${ogv_src}"></source>
  <source type="video/webm" src="${webm_src}"></source>
</video>
                    `;
                    range.insertNode(stateElement);
                  } else if (mitem.match(/mp3$/)) {
                    const stateElement = document.createElement("audio");
                    stateElement.controls = "controls";
                    const mp3_src = mitem;
                    const ogg_src = mitem.replace(/mp3$/, "ogg");
                    const aac_src = mitem.replace(/mp3$/, "aac");
                    stateElement.innerHTML = `
<source type="audio/mp3" src="${mp3_src}"></source>
<source type="audio/ogg" src="${ogg_src}"></source>
<source type="audio/aac" src="${aac_src}"></source>
                    `;
                    range.insertNode(stateElement);
                  } else {
                    const stateElement = document.createElement("div");
                    stateElement.setAttribute("contenteditable", true);
                    stateElement.classList.add("resize-media");
                    stateElement.innerHTML = `
<img src="${mitem}" />
                    `;
                    range.insertNode(stateElement);
                    console.log("image", stateElement);
                  }

                }
              } else {
                const stateElement = document.createElement(state_elems[state]);
                if (state == "link") {
                  stateElement.href = prompt("Link URL:");
                  stateElement.target = "_blank";
                }
                if (range.startOffset === range.endOffset) {
                  console.log("insert", stateElement);
                  range.insertNode(stateElement);
                } else {
                  range.surroundContents(stateElement);
                }
              }
            }
          }
        }
      })
    }

    this.element.appendChild(element);

  }
}
