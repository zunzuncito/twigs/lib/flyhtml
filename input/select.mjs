


export default class SelectInput {
  constructor(element, cfg) {
    if (!cfg) cfg = {};
    this.element = element;

    const _this = this;
    
    this.options_wrap = document.createElement("div");
    this.options_wrap.classList.add("select-input-wrap");

    this.input = document.createElement("input");
    this.input.type = "text";
    this.input.setAttribute("name", element.getAttribute("name"));
    this.input.placeholder = element.getAttribute("placeholder");

    const filter_and_add_options = (e) => {
      const added = false;
      _this.options_block.innerHTML = "";
      const sqstr = _this.input.value.toLowerCase();
      let found = false;
      for (let option of _this.options) {
        const elem = option.element;
        const lower_val = elem.innerText.toLowerCase();
        if (lower_val.includes(sqstr) && lower_val !== sqstr) {
          _this.options_block.appendChild(elem);
          found = true;
        } else if (lower_val === sqstr) {
          _this.input.real_value = elem.value;
        }
      }

      if (found && !_this.options_wrap.contains(_this.options_block)) {
        _this.options_wrap.appendChild(_this.options_block);
      } else if (!found && _this.options_wrap.contains(_this.options_block)) {
        _this.options_wrap.removeChild(_this.options_block);
      }
    }

    this.input.addEventListener("keyup", filter_and_add_options);
    this.input.addEventListener("keyup", (e) => {
      for (let valid of _this.valid_text) {
        if (valid.value == e.target.real_value) {
          e.target.real_value = valid.value;
          return;
        }
      }
      e.target.real_value = undefined;
    });


    this.options_wrap.appendChild(this.input);

    this.options_block = document.createElement("div");
    this.options_block.classList.add("select-input-block");


    this.valid_values = [];
    this.valid_text = [];
    this.options = [];
    for (const option of element.children) {
      if (!option.disabled) {
        if (option.selected) {
          _this.input.value = option.innerText;
          _this.input.real_value = option.value;
        }
        const option_element = document.createElement("div");
        option_element.classList.add("select-input-option");
        option_element.innerHTML = option.innerHTML;
        option_element.value = option.value;
        this.valid_values.push(option.value);
        this.valid_text.push({
          text: option.innerHTML,
          value: option.value
        });
        option_element.addEventListener("mousedown", (e) => {
          this.input.value = option_element.innerText;
          this.input.real_value = option_element.value;
          const event = new Event("keyup");
          this.input.dispatchEvent(event);
          const ch_event = new Event("change");
          this.input.dispatchEvent(ch_event);
        });
        this.options.push({
          element: option_element
        });
      }
    }


    if (cfg.dont_hide) {
      filter_and_add_options({
        target: this.input
      });
    } else {
      this.input.addEventListener("focus", (e) => {
        filter_and_add_options(e);
      });

      this.input.addEventListener("blur", (e) => {
        if (_this.options_wrap.contains(_this.options_block)) {
          _this.options_wrap.removeChild(_this.options_block);
        }
      });
    }


    element.parentNode.replaceChild(this.options_wrap, element);
  }

  validate(value) {
    return this.valid_values.includes(value);
  }
}
