
export default class SelectGridInput {
  constructor(cfg) {
    if (!cfg) cfg = {};
    const _this = this;

    this.multifile = cfg.multifile;

    this.value = '';

    this.values = [];

    this.items = [];

    this.prev_values = [ ...this.values ];

    this.element = document.createElement("div");
    this.element.classList.add("select-grid-div");
/*
    if (!document.body.contains(this.element)) {
      document.body.appendChild(this.element);
    }*/
  }

  async display() {
    if (!document.body.contains(this.element)) {
      document.body.appendChild(this.element);
    }

    this.last_selected = undefined;

    if (typeof this.on_focus == "function") await this.on_focus();
  }

  hide() {
    this.element.parentNode.removeChild(this.element);
  }

  select(grid_item) {
    const multiple = this.multifile;
    const nval = grid_item.value || grid_item.getAttribute("value");
    if (multiple) {
      if (this.values.includes(nval)) {
        this.values.splice(this.values.indexOf(nval), 1);
        grid_item.classList.remove("selected");
      } else {
        this.values.push(nval);
        grid_item.classList.add("selected");
      }
    } else {
      this.values = [nval];
      if (this.last_selected) this.last_selected.classList.remove("selected");
      grid_item.classList.add("selected");
      this.last_selected = grid_item;
    }
    this.value = this.values.join(", ");
  }

  select_all() {
    this.values = [];
    this.value = "";
    for (let grid_item of this.items) {
      this.select(grid_item);
    }
  }

  deselect_all() {
    this.values = [];
    this.value = "";
    for (let grid_item of this.items) {
      if (grid_item.classList.contains("selected")) {
        grid_item.classList.remove("selected");
      }
    }
  }

  add_item(grid_item) {
    const _this = this;
    const multiple = this.multifile;
    const nval = grid_item.value || grid_item.getAttribute("value");
    const listener_fn = (e) => {
      _this.select(grid_item);
    }

    this.items.push(grid_item);

//    listener.element.classList.remove("selected");

    if (!_this.values.includes(nval)) {
      grid_item.classList.remove("selected");
    } else {
      grid_item.classList.add("selected");
      if (!multiple) _this.last_selected = grid_item;
    }

    grid_item.addEventListener("click", listener_fn);
  }

  clear() {
    this.items = [];
  }

  static get_current_instance() {
    return current_instance;
  }
}
