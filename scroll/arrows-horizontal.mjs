
import Animator from 'zunzun/animator/index.mjs'


export default class ArrowsHorizontal {
  constructor(element, step, duration) {
    this.element = element;

    this.wrap = document.createElement("div");
    this.wrap.classList.add("scroll-arrow-wrap");

    this.left = document.createElement("div");
    this.left.addEventListener("click", (e) => {
      Animator.transition(
        element,
        element.scrollLeft,
        element.scrollLeft - step,
        duration,
        (val) => {
          element.scrollTo(val, 0);
        }, () => {
          //done
        }
      );
      
    });
    this.left.classList.add("scroll-arrow-left");
    this.left.innerText = "<";
    this.wrap.appendChild(this.left);
    this.right = document.createElement("div");
    this.right.addEventListener("click", (e) => {
      Animator.transition(
        element,
        element.scrollLeft,
        element.scrollLeft + step,
        duration,
        (val) => {
          element.scrollTo(val, 0);
        }, () => {
          //done
        }
      );
    });
    this.right.classList.add("scroll-arrow-right");
    this.right.innerText = ">";
    this.wrap.appendChild(this.right);

    element.parentNode.replaceChild(this.wrap, element);
    this.wrap.appendChild(element);
  }
}
