

export default class ContentTableRow {

  constructor(values, table) {
    this.values = values;
    this.table = table;
  }

  set_elements(elements) {
    this.elements = elements;
  }


  update(values) {
    console.log("values", values);
    console.log("elements", this.elements);
    for (let key in values) {
      if (typeof values[key] !== "undefined") {
        this.values[key] = values[key];
        console.log(key, values[key]);
        this.elements[key].innerHTML = typeof values[key] == "object" ? values[key][this.table.current_language] : values[key];
      }
    }
  }

}
