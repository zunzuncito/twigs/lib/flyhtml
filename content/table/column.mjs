

export default class ContentTableColumn {

  constructor(cfg, table) {
    this.name = cfg.name;
    this.type = cfg.type;

    this.table = table;

    this.cfg = cfg;

    this.element = document.createElement('div');
    this.element.classList.add("column");
    this.element.classList.add(cfg.type);

    this.th = document.createElement('div');
    this.th.classList.add("th");
    this.th.innerHTML = cfg.title || cfg.name;

    this.current_width = undefined;

    if (cfg.resize) {
      this.resize_e = document.createElement('div');
      this.resize_e.classList.add("resize");
      
      let resizing = false;
      this.resize_e.addEventListener("mousedown", (e) => {
        resizing = true;
        table.element.classList.add("no-select");
      });


      const _this = this;
      window.addEventListener("mousemove", (e) => {
        if (resizing) {
          const next_column = table.get_next_column(_this);
          const parent_element = _this.element.parentElement;
          const offset_width = parent_element.offsetWidth;
          const scroll_width = parent_element.scrollWidth;

          if (typeof _this.current_width == "undefined") _this.current_width = _this.element.offsetWidth;

          if (scroll_width == offset_width) {

            if (parent_element == next_column.element.parentElement) {
              _this.current_width += e.movementX;
              next_column.current_width -= e.movementX;
              if (next_column.current_width < 64) next_column.current_width = 64;
            } else {
              if (e.movementX > 0) {
                _this.current_width += e.movementX;
              }
            }
          } else {
            if (e.movementX > 0) {
              _this.current_width += e.movementX;
            } else {
              const diff = scroll_width - offset_width;
              _this.current_width += diff < e.movementX*-1 ? diff*-1 : e.movementX;
            }

          }


          if (_this.current_width < 64) _this.current_width = 64;


          console.log("_this.current_width", _this.current_width);
          console.log("next_column.current_width", next_column.current_width);
          _this.element.style.width = _this.current_width+"px";
          next_column.element.style.width = next_column.current_width+"px";
  
        }

      });
      window.addEventListener("mouseup", (e) => {
        resizing = false;
        table.element.classList.remove("no-select");
      });
      this.th.appendChild(this.resize_e);
    }

    this.element.appendChild(this.th);

  }
  
  clear() {
    this.element.innerHTML = "";
    this.element.appendChild(this.th);
  }

  add(val, row) {
    const e_val = document.createElement('div');
    e_val.classList.add("td");
    if (typeof this.cfg.value_cb == "function") {
      e_val.appendChild(this.cfg.value_cb(row));
    } else {
      if (val) {
        if (typeof val == "object" && typeof val.display == "string") { 
          e_val.innerHTML = val.display;
        } else {
          e_val.innerHTML = val;
        }
      } else {
        e_val.innerHTML = "";
      }
    }

    this.element.appendChild(e_val);
    return e_val;
  }

  resize() {
    this.current_width = this.element.offsetWidth;
    this.element.style.width = this.current_width+"px";
  }

}
