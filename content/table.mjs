
import ContentDisplay from './display.mjs'

import ContentTableColumn from './table/column.mjs'
import ContentTableRow from './table/row.mjs'

export default class ContentTable extends ContentDisplay {

  constructor(name, columns, cfg) {
    super(name, columns, cfg)

    this.element = document.createElement('div');
    this.element.classList.add("flyhtml-table");

    this.current_language = cfg.language;

    this.table_columns = [];

    this.wrap_e = document.createElement('div');
    this.wrap_e.classList.add("wrap");
    for (let col_cfg of this.columns) {
      const ctc = new ContentTableColumn(col_cfg, this);
      this.table_columns.push(ctc);
      if (col_cfg.wrap) {
        this.wrap_e.appendChild(ctc.element);
        if (!this.element.contains(this.wrap_e)) {
          this.element.appendChild(this.wrap_e);
        }
      } else {
        this.element.appendChild(ctc.element);
      }
    }

    this.rows = [];
  }

  clear() {
    this.rows = [];
    for (let ctc of this.table_columns) {
      ctc.clear();
    }
  }

  add(row) {
    const table_row = new ContentTableRow(row, this);
    const row_e = {};
    for (let ctc of this.table_columns) {
      if (ctc.cfg.lang) {
        row[ctc.name].display = row[ctc.name][this.current_language];
      }
      if (typeof ctc.cfg.references == "string" && ctc.cfg.option_text && row[ctc.name]) {
        row[ctc.name] = row[ctc.name].text;
      }
      row_e[ctc.name] = ctc.add(row[ctc.name], table_row);
    }
    table_row.set_elements(row_e);

    this.rows.push(table_row);

  }

  get_column(name) {
    for (let column of this.columns) {
      if (column.name === name) {
        return column;
      }
    }
  }

  get_next_column(col) {
    const coli = this.table_columns.indexOf(col);
    if (coli < this.table_columns.length-1) {
      const next_column_index = coli + 1;
      return this.table_columns[next_column_index]
    } else {
      return undefined;
    }
  }

  get_prev_column(col) {
    const coli = this.table_columns.indexOf(col);
    if (coli > 0) {
      const next_column_index = coli - 1;
      return this.table_columns[next_column_index]
    } else {
      return undefined;
    }
  }

  fit() {

    let total_columns_width = 0;
    let total_wrapped = 0;
    for (let ctc of this.table_columns) {
      if (ctc.element.parentElement == this.wrap_e) {
        total_wrapped++;
        total_columns_width += ctc.current_width;
      }
    }

    if (total_columns_width < this.wrap_e.offsetWidth) {
      const diff = this.wrap_e.offsetWidth - total_columns_width;


      for (let ctc of this.table_columns) {
        if (ctc.element.parentElement == this.wrap_e) {
          ctc.current_width += diff/total_wrapped;
          ctc.element.style.width = ctc.current_width+"px";
        }
      }
    }
  }

  prepare() {
    for (let ctc of this.table_columns) {
      ctc.resize();
    }
  }

}
